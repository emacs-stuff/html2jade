;;; html2jade.el --- Transform html to jade.

;; Copyright (C) 2014-2015 vindarel

;; Author: vindarel <ehvince@mailz.org>
;; Keywords: html jade
;; Version: 0.1
;; X-Original-Version: 0.1
;; URL: https://gitlab.com/emacs-stuff/html2jade

;; This file is not part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;; Package-Requires: (())

;;; Commentary:

;; Replace the active region or the current line by its jade equivalent.

;;; Requirements:

;; `html2jade' command line tool, see https://github.com/donpark/html2jade
;; Install it globally: npm install -g html2jade

;;; Usage:

;; M-x html2jade, on the current line or region.

;;; Code:

(defun html2jade-line ()
  "Converts the given html snippet to jade and insert it at point."
  (let ((html (current-line)))
    (kill-whole-line)
    (insert (shell-command-to-string (format "echo '%s' | html2jade --bodyless -" html)))
    (previous-line)
    (indent-according-to-mode)))

(defun html2jade-region (beg end)
  "Converts the html of the region to jade."
  (let ((html (buffer-substring-no-properties beg end)))
    (kill-region beg end)
    (insert (shell-command-to-string (format "echo '%s' | html2jade --bodyless -" html)))
    (indent-region beg end)))

(defun html2jade (beg end)
  "Converts the line or the region from html to jade."
  (interactive "r")
  (if (region-active-p)
      (html2jade-region beg end)
    (html2jade-line)))

(provide 'html2jade)
;;; html2jade.el ends here
